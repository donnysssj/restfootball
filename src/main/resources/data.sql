INSERT INTO COUNTRY(name, language) VALUES('Congo', 'fr');
INSERT INTO COUNTRY(name, language) VALUES('France', 'fr');
INSERT INTO COUNTRY(name, language) VALUES('Canada', 'fr');
INSERT INTO COUNTRY(name, language) VALUES('Madagascar', 'fr');
INSERT INTO COUNTRY(name, language) VALUES('Cameroon', 'fr');
INSERT INTO COUNTRY(name, language) VALUES('Italy', 'it');
INSERT INTO COUNTRY(name, language) VALUES('Belgium', 'ge');
INSERT INTO COUNTRY(name, language) VALUES('Austria', 'ge');
INSERT INTO COUNTRY(name, language) VALUES('Switzerland', 'ge');
INSERT INTO COUNTRY(name, language) VALUES('Luxembourg', 'ge');
INSERT INTO COUNTRY(name, language) VALUES('Liechtenstein', 'ge');
INSERT INTO COUNTRY(name, language) VALUES('Mexico', 'es');
INSERT INTO COUNTRY(name, language) VALUES('Colombia', 'es');
INSERT INTO COUNTRY(name, language) VALUES('Spain', 'es');
INSERT INTO COUNTRY(name, language) VALUES('Australia', 'en');
INSERT INTO COUNTRY(name, language) VALUES('New Zealand', 'en');

INSERT INTO LEAGUE(name, country_id) VALUES('League1', 1);

//INSERT INTO TEAM(name, league_id) VALUES('Team1', 17);