package de.planerio.developertest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import de.planerio.developertest.validation.CountryLanguageConstraint;

@Entity
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false, unique = true)
	private String name;

	@Column(nullable = false)
	@CountryLanguageConstraint
	private String language;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Country withId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country withName(String name) {
		this.name = name;
		return this;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Country withLanguage(String language) {
		this.language = language;
		return this;
	}
}
