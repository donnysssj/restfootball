package de.planerio.developertest.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import de.planerio.developertest.dto.PlayerDto;
import de.planerio.developertest.dto.TeamDto;
import de.planerio.developertest.model.Player;
import de.planerio.developertest.repository.PlayerRepository;

@Service
public class PlayerService {

	private static final int MAX_NUMBER_OF_PLAYER_IN_A_TEAM = 25;

	private PlayerRepository playerRepository;
	private TeamService teamService;

	public PlayerService(PlayerRepository playerRepository, TeamService teamService) {
		this.playerRepository = playerRepository;
		this.teamService = teamService;
	}

	public PlayerDto getPlayer(long playerId) {
		return new PlayerDto(playerRepository.findById(playerId).orElseThrow());
	}

	public List<PlayerDto> getPlayers(long teamId) {
		return PlayerDto.convert(playerRepository.findByTeamId(teamId));
	}

	public Page<PlayerDto> getPlayers(Pageable pageable, String position) {
		Page<Player> players;
		if (position != null) {
			players = playerRepository.findByPosition(pageable, position);
		} else {
			players = playerRepository.findAll(pageable);
		}

		return PlayerDto.convert(players);
	}

	public PlayerDto createPlayer(Player player) {
		return new PlayerDto(playerRepository.save(player));
	}

	public List<PlayerDto> createPlayers(List<Player> player) {
		return PlayerDto.convert(playerRepository.saveAll(player));
	}

	public void deletePlayer(long playerId) {
		playerRepository.deleteById(playerId);
	}

	public PlayerDto updatePlayer(Player updatedPlayer, long playerId) {
		playerRepository.findById(playerId).orElseThrow();
		return new PlayerDto(playerRepository.save(updatedPlayer));
	}

	public void validatePlayer(Player player) {
		long teamId = player.getTeam().getId();
		validateTeamSize(teamId);
		validateShirtNumber(player, teamId);
	}

	public void validatePlayers(List<Player> players) {
		players.stream().forEach(player -> validatePlayer(player));
	}

	public void validateUpdatedPlayer(Player updatedPlayer) {
		PlayerDto currentPlayer = getPlayer(updatedPlayer.getId());
		if (updatedPlayer.getShirtNumber() != currentPlayer.getShirtNumber()) {
			validatePlayer(updatedPlayer);
		}
	}

	private void validateShirtNumber(Player player, long teamId) {
		List<PlayerDto> teamPlayers = getPlayers(teamId);
		List<Integer> teamUsedShirtNumber = teamPlayers.stream().map(PlayerDto::getShirtNumber)
				.collect(Collectors.toList());

		int playerShirtNumber = player.getShirtNumber();
		if (teamUsedShirtNumber.contains(playerShirtNumber)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"The informed player shirt number is not available.");
		}
	}

	private void validateTeamSize(long teamId) {
		TeamDto team = teamService.getTeam(teamId);
		if (team.getPlayers().size() >= MAX_NUMBER_OF_PLAYER_IN_A_TEAM) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"There cannot be more than " + MAX_NUMBER_OF_PLAYER_IN_A_TEAM + " players in a team.");
		}
	}
}
