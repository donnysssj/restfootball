package de.planerio.developertest.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import de.planerio.developertest.dto.LeagueDto;
import de.planerio.developertest.dto.TeamDto;
import de.planerio.developertest.model.Team;
import de.planerio.developertest.repository.TeamRepository;

@Service
public class TeamService {

	private static final int MAX_NUMBER_OF_TEAMS_IN_A_LEAGUE = 20;

	private TeamRepository teamRepository;
	private LeagueService leagueService;

	public TeamService(TeamRepository teamRepository, LeagueService leagueService) {
		this.teamRepository = teamRepository;
		this.leagueService = leagueService;
	}

	public TeamDto getTeam(long teamId) {
		return new TeamDto(teamRepository.findById(teamId).orElseThrow());
	}

	public Page<TeamDto> getTeams(Pageable pageable) {
		return TeamDto.convert(teamRepository.findAll(pageable));
	}

	public TeamDto createTeam(Team team) {
		return new TeamDto(teamRepository.save(team));
	}

	public List<TeamDto> createTeams(List<Team> team) {
		return TeamDto.convert(teamRepository.saveAll(team));
	}

	public void deleteTeam(long teamId) {
		teamRepository.deleteById(teamId);
	}

	public TeamDto updateTeam(Team updatedTeam, long teamId) {
		teamRepository.findById(teamId).orElseThrow();
		return new TeamDto(teamRepository.save(updatedTeam));
	}

	public void validateTeam(Team team) {
		long leagueId = team.getLeague().getId();
		validateLeagueSize(leagueId);
	}

	public void validateTeams(List<Team> teams) {
		teams.stream().forEach(team -> validateTeam(team));
	}

	public void validateUpdatedTeam(Team updatedTeam) {
		TeamDto currentTeam = getTeam(updatedTeam.getId());
		if (updatedTeam.getLeague().getId() != currentTeam.getLeague().getId()) {
			validateTeam(updatedTeam);
		}
	}

	private void validateLeagueSize(long leagueId) {
		LeagueDto league = leagueService.getLeague(leagueId);
		if (league.getTeams().size() >= MAX_NUMBER_OF_TEAMS_IN_A_LEAGUE) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"There cannot be more than " + MAX_NUMBER_OF_TEAMS_IN_A_LEAGUE + " teams per league.");
		}
	}
}
