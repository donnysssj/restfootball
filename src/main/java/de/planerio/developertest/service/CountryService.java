package de.planerio.developertest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import de.planerio.developertest.dto.CountryDto;
import de.planerio.developertest.model.Country;
import de.planerio.developertest.repository.CountryRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;

	public CountryDto getCountry(long countryId) {
		return new CountryDto(countryRepository.findById(countryId).orElseThrow());
	}

	public Page<CountryDto> getCountries(Pageable pageable) {
		return CountryDto.convert(countryRepository.findAll(pageable));
	}

	public CountryDto createCountry(Country country) {
		return new CountryDto(countryRepository.save(country));
	}

	public List<CountryDto> createCountries(List<Country> country) {
		return CountryDto.convert(countryRepository.saveAll(country));
	}

	public void deleteCountry(long countryId) {
		countryRepository.deleteById(countryId);
	}

	public CountryDto updateCountry(Country updatedCountry, long countryId) {
		countryRepository.findById(countryId).orElseThrow();
		return new CountryDto(countryRepository.save(updatedCountry));
	}
}
