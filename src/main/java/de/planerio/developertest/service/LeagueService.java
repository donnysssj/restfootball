package de.planerio.developertest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import de.planerio.developertest.dto.LeagueDto;
import de.planerio.developertest.model.League;
import de.planerio.developertest.repository.LeagueRepository;

@Service
public class LeagueService {

	@Autowired
	private LeagueRepository leagueRepository;

	public LeagueDto getLeague(long leagueId) {
		return new LeagueDto(leagueRepository.findById(leagueId).orElseThrow());
	}

	public List<LeagueDto> getLeagues() {
		return LeagueDto.convert(leagueRepository.findAll());
	}

	public List<LeagueDto> getLeagues(long countryId) {
		return LeagueDto.convert(leagueRepository.findByCountryId(countryId));
	}

	public Page<League> getLeagues(Pageable pageable, String language) {
		if (language != null) {
			return leagueRepository.findByCountryLanguage(pageable, language);
		}

		return leagueRepository.findAll(pageable);
	}

	public LeagueDto createLeague(League league) {
		return new LeagueDto(leagueRepository.save(league));
	}

	public List<LeagueDto> createLeagues(List<League> league) {
		return LeagueDto.convert(leagueRepository.saveAll(league));
	}

	public void deleteLeague(long leagueId) {
		leagueRepository.deleteById(leagueId);
	}

	public LeagueDto updateLeague(League updatedLeague, long leagueId) {
		leagueRepository.findById(leagueId).orElseThrow();
		return new LeagueDto(leagueRepository.save(updatedLeague));
	}

	public void validateLeagues(List<League> leagues) {
		leagues.stream().forEach(league -> validateLeague(league));
	}

	public void validateLeague(League league) {
		List<LeagueDto> countryLeagues = getLeagues(league.getCountry().getId());
		if (!countryLeagues.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"There cannot be more than one league per country.");
		}
	}

	public void validateUpdatedLeague(League updatedLeague) {
		LeagueDto currentLeague = getLeague(updatedLeague.getId());
		if (updatedLeague.getCountry().getId() != currentLeague.getCountry().getId()) {
			validateLeague(updatedLeague);
		}
	}
}
