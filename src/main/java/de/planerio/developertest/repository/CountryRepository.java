package de.planerio.developertest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.planerio.developertest.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
