package de.planerio.developertest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import de.planerio.developertest.model.League;

public interface LeagueRepository extends JpaRepository<League, Long> {

	List<League> findByCountryId(long countryId);

	Page<League> findByCountryLanguage(Pageable pageable, String language);
}
