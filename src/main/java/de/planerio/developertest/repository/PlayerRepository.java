package de.planerio.developertest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import de.planerio.developertest.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

	List<Player> findByTeamId(long teamId);

	Page<Player> findByPosition(Pageable pageable, String position);
}
