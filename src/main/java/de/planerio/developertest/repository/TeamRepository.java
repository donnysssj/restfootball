package de.planerio.developertest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import de.planerio.developertest.model.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
