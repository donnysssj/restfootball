package de.planerio.developertest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.planerio.developertest.dto.TeamDto;
import de.planerio.developertest.model.Team;
import de.planerio.developertest.service.TeamService;

@RestController
public class TeamController {

	@Autowired
	private TeamService teamService;

	@PostMapping("/team")
	public ResponseEntity<TeamDto> createTeam(@RequestBody Team team, UriComponentsBuilder uriBuilder) {
		teamService.validateTeam(team);
		TeamDto createdTeam = teamService.createTeam(team);

		URI uri = uriBuilder.path("/team/{id}").buildAndExpand(createdTeam.getId()).toUri();
		return ResponseEntity.created(uri).body(createdTeam);
	}

	@PostMapping("/teams")
	public ResponseEntity<List<TeamDto>> createTeams(@RequestBody List<Team> team) {
		teamService.validateTeams(team);
		return ResponseEntity.created(null).body(teamService.createTeams(team));
	}

	@GetMapping("/team/{teamId}")
	public ResponseEntity<TeamDto> getTeam(@PathVariable long teamId) {
		return ResponseEntity.ok(teamService.getTeam(teamId));
	}

	@GetMapping("/team")
	public Page<TeamDto> getTeams(@PageableDefault(page = 0, size = 5) Pageable pageable) {
		return teamService.getTeams(pageable);
	}

	@PutMapping("/team/{teamId}")
	public ResponseEntity<TeamDto> updatedTeam(@RequestBody Team updatedTeam, @PathVariable long teamId) {
		teamService.validateUpdatedTeam(updatedTeam);
		return ResponseEntity.ok(teamService.updateTeam(updatedTeam, teamId));
	}

	@DeleteMapping("/team/{teamId}")
	public ResponseEntity<?> deleteTeam(@PathVariable long teamId) {
		teamService.deleteTeam(teamId);
		return ResponseEntity.noContent().build();
	}
}
