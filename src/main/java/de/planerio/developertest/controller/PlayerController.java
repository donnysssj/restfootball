package de.planerio.developertest.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.planerio.developertest.dto.PlayerDto;
import de.planerio.developertest.model.Player;
import de.planerio.developertest.service.PlayerService;

@RestController
public class PlayerController {

	private PlayerService playerService;

	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}

	@PostMapping("/player")
	public ResponseEntity<PlayerDto> createPlayer(@RequestBody @Valid Player player, UriComponentsBuilder uriBuilder) {
		playerService.validatePlayer(player);
		PlayerDto createdPlayer = playerService.createPlayer(player);

		URI uri = uriBuilder.path("/player/{id}").buildAndExpand(createdPlayer.getId()).toUri();
		return ResponseEntity.created(uri).body(createdPlayer);
	}

	@PostMapping("/players")
	public ResponseEntity<List<PlayerDto>> createPlayers(@RequestBody List<Player> players) {
		playerService.validatePlayers(players);
		return ResponseEntity.created(null).body(playerService.createPlayers(players));
	}

	@GetMapping("/player/{playerId}")
	public ResponseEntity<PlayerDto> getPlayer(@PathVariable long playerId) {
		return ResponseEntity.ok(playerService.getPlayer(playerId));
	}

	@GetMapping("/player")
	ResponseEntity<Page<PlayerDto>> getPlayers(@PageableDefault(page = 0, size = 5) Pageable pageable,
			@RequestParam(required = false) String position) {
		return ResponseEntity.ok(playerService.getPlayers(pageable, position));
	}

	@PutMapping("/player/{playerId}")
	public ResponseEntity<PlayerDto> updatedPlayer(@RequestBody Player updatedPlayer, @PathVariable long playerId) {
		playerService.validateUpdatedPlayer(updatedPlayer);
		return ResponseEntity.ok(playerService.updatePlayer(updatedPlayer, playerId));
	}

	@DeleteMapping("/player/{playerId}")
	public ResponseEntity<?> deletePlayer(@PathVariable long playerId) {
		playerService.deletePlayer(playerId);
		return ResponseEntity.noContent().build();
	}
}
