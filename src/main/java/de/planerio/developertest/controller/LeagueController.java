package de.planerio.developertest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.planerio.developertest.dto.LeagueDto;
import de.planerio.developertest.model.League;
import de.planerio.developertest.service.LeagueService;

@RestController
public class LeagueController {

	@Autowired
	private LeagueService leagueService;

	@PostMapping("/league")
	public ResponseEntity<LeagueDto> createLeague(@RequestBody League league, UriComponentsBuilder uriBuilder) {
		leagueService.validateLeague(league);
		LeagueDto createdLeague = leagueService.createLeague(league);

		URI uri = uriBuilder.path("/league/{id}").buildAndExpand(createdLeague.getId()).toUri();
		return ResponseEntity.created(uri).body(createdLeague);
	}

	@PostMapping("/leagues")
	public ResponseEntity<List<LeagueDto>> createLeagues(@RequestBody List<League> leagues) {
		leagueService.validateLeagues(leagues);
		return ResponseEntity.created(null).body(leagueService.createLeagues(leagues));
	}

	@GetMapping("/league/{leagueId}")
	public ResponseEntity<LeagueDto> getLeague(@PathVariable long leagueId) {
		return ResponseEntity.ok(leagueService.getLeague(leagueId));
	}

	@GetMapping("/league")
	public Page<League> getLeagues(@PageableDefault(page = 0, size = 5) Pageable pageable,
			@RequestParam(required = false) String language) {
		return leagueService.getLeagues(pageable, language);
	}

	@PutMapping("/league/{leagueId}")
	public ResponseEntity<LeagueDto> updatedLeague(@RequestBody League updatedLeague, @PathVariable long leagueId) {
		leagueService.validateUpdatedLeague(updatedLeague);
		return ResponseEntity.ok(leagueService.updateLeague(updatedLeague, leagueId));
	}

	@DeleteMapping("/league/{leagueId}")
	public ResponseEntity<?> deleteLeague(@PathVariable long leagueId) {
		leagueService.deleteLeague(leagueId);
		return ResponseEntity.noContent().build();
	}
}
