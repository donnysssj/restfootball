package de.planerio.developertest.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.planerio.developertest.dto.CountryDto;
import de.planerio.developertest.model.Country;
import de.planerio.developertest.service.CountryService;

@Validated
@RestController
public class CountryController {

	@Autowired
	private CountryService countryService;

	@PostMapping("/country")
	public ResponseEntity<CountryDto> createCountry(@RequestBody @Valid Country country,
			UriComponentsBuilder uriBuilder) {
		CountryDto createdCountry = countryService.createCountry(country);

		URI uri = uriBuilder.path("/country/{id}").buildAndExpand(createdCountry.getId()).toUri();
		return ResponseEntity.created(uri).body(createdCountry);
	}

	@PostMapping("/countries")
	public ResponseEntity<List<CountryDto>> createCountries(@RequestBody List<@Valid Country> countries) {
		return ResponseEntity.created(null).body(countryService.createCountries(countries));
	}

	@GetMapping("/country/{countryId}")
	public ResponseEntity<CountryDto> getCountry(@PathVariable long countryId) {
		return ResponseEntity.ok(countryService.getCountry(countryId));
	}

	@GetMapping("/country")
	public ResponseEntity<Page<CountryDto>> getCountries(@PageableDefault(page = 0, size = 5) Pageable pageable) {
		Page<CountryDto> countries = countryService.getCountries(pageable);
		return ResponseEntity.ok(countries);
	}

	@PutMapping("/country/{countryId}")
	public ResponseEntity<CountryDto> updatedCountry(@RequestBody Country updatedCountry,
			@PathVariable long countryId) {
		return ResponseEntity.ok(countryService.updateCountry(updatedCountry, countryId));
	}

	@DeleteMapping("/country/{countryId}")
	public ResponseEntity<?> deleteCountry(@PathVariable long countryId) {
		countryService.deleteCountry(countryId);
		return ResponseEntity.noContent().build();
	}
}
