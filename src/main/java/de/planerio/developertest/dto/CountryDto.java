package de.planerio.developertest.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import de.planerio.developertest.model.Country;

public class CountryDto {

	private long id;
	private String name;
	private String language;

	public CountryDto() {
	}

	public CountryDto(Country country) {
		this.id = country.getId();
		this.name = country.getName();
		this.language = country.getLanguage();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public static List<CountryDto> convert(List<Country> countries) {
		return countries.stream().map(country -> new CountryDto(country)).collect(Collectors.toList());
	}

	public static Page<CountryDto> convert(Page<Country> countries) {
		return countries.map(CountryDto::new);
	}
}
