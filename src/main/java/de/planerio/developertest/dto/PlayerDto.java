package de.planerio.developertest.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import de.planerio.developertest.model.Player;

public class PlayerDto {

	private long id;
	private String name;
	private TeamDto team;
	private String position;
	private int shirtNumber;

	public PlayerDto() {
	}

	public PlayerDto(Player player) {
		this.id = player.getId();
		this.name = player.getName();
		this.team = new TeamDto(player.getTeam());
		this.position = player.getPosition();
		this.shirtNumber = player.getShirtNumber();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TeamDto getTeam() {
		return team;
	}

	public void setTeam(TeamDto team) {
		this.team = team;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getShirtNumber() {
		return shirtNumber;
	}

	public void setShirtNumber(int shirtNumber) {
		this.shirtNumber = shirtNumber;
	}

	public static List<PlayerDto> convert(List<Player> players) {
		return players.stream().map(player -> new PlayerDto(player)).collect(Collectors.toList());
	}

	public static Page<PlayerDto> convert(Page<Player> players) {
		return players.map(PlayerDto::new);
	}
}
