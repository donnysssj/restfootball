package de.planerio.developertest.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import de.planerio.developertest.model.Country;
import de.planerio.developertest.model.League;

public class LeagueDto {

	private long id;
	private String name;
	private Country country;

	private List<TeamDto> teams;

	public LeagueDto(League league) {
		this.id = league.getId();
		this.name = league.getName();
		this.country = league.getCountry();
		this.teams = TeamDto.convert(league.getTeams());
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Country getCountry() {
		return country;
	}

	public List<TeamDto> getTeams() {
		return teams;
	}

	public static List<LeagueDto> convert(List<League> leagues) {
		return leagues.stream().map(league -> new LeagueDto(league)).collect(Collectors.toList());
	}

	public static Page<LeagueDto> convert(Page<League> leagues) {
		return leagues.map(LeagueDto::new);
	}
}
