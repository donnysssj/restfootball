package de.planerio.developertest.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import de.planerio.developertest.model.Team;

public class TeamDto {

	private long id;
	private String name;
	private LeagueDto league;
	private List<PlayerDto> players;

	public TeamDto(Team team) {
		this.id = team.getId();
		this.name = team.getName();
		this.league = new LeagueDto(team.getLeague());
		this.players = PlayerDto.convert(team.getPlayers());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LeagueDto getLeague() {
		return league;
	}

	public void setTeam(LeagueDto league) {
		this.league = league;
	}

	public List<PlayerDto> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerDto> players) {
		this.players = players;
	}

	public static List<TeamDto> convert(List<Team> teams) {
		return teams.stream().map(team -> new TeamDto(team)).collect(Collectors.toList());
	}

	public static Page<TeamDto> convert(Page<Team> teams) {
		return teams.map(TeamDto::new);
	}
}
