package de.planerio.developertest.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PlayerPositionValidator.class)
public @interface PlayerPositionConstraint {

	String message() default "The informed player position is not supported.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
