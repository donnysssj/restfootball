package de.planerio.developertest.validation;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PlayerPositionValidator implements ConstraintValidator<PlayerPositionConstraint, String> {

	@Override
	public void initialize(PlayerPositionConstraint playerPositionConstraint) {
	}

	@Override
	public boolean isValid(String playerPosition, ConstraintValidatorContext constraintValidatorContext) {
		return Arrays.asList("GK", "CB", "RB", "LB", "LWB", "RWB", "CDM", "CM", "LM", "RM", "CAM", "ST", "CF")
				.contains(playerPosition);
	}
}
