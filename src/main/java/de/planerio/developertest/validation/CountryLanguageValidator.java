package de.planerio.developertest.validation;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CountryLanguageValidator implements ConstraintValidator<CountryLanguageConstraint, String> {

	@Override
	public void initialize(CountryLanguageConstraint countryLanguageConstraint) {
	}

	@Override
	public boolean isValid(String countryLanguage, ConstraintValidatorContext constraintValidatorContext) {
		return Arrays.asList("de", "fr", "en", "es", "it").contains(countryLanguage);
	}
}
