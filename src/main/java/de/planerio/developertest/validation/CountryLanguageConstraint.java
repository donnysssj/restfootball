package de.planerio.developertest.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ ElementType.METHOD, ElementType.TYPE_USE, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CountryLanguageValidator.class)
public @interface CountryLanguageConstraint {

	String message() default "The informed language is not supported.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
