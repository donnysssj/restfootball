package de.planerio.developertest.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.planerio.developertest.dto.PlayerDto;
import de.planerio.developertest.service.PlayerService;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class PlayerControllerTest {

	private static final long PLAYER_ID = 1;
	private static final String PLAYER_NAME = "playerName1";
	private static final String PLAYER_POSITION = "GK";
	private static final int PLAYER_SHIRT_NUMBER = 1;

	private PlayerController playerController;

	@Mock
	private PlayerService playerService;

	@BeforeEach
	void beforeClass() {
		playerController = new PlayerController(playerService);
	}

	@Test
	void getPlayer_withValidId_sholdReturnPlayerDto() throws Exception {
		PlayerDto servicePlayer = new PlayerDto();
		servicePlayer.setId(PLAYER_ID);
		servicePlayer.setName(PLAYER_NAME);
		servicePlayer.setPosition(PLAYER_POSITION);
		servicePlayer.setShirtNumber(PLAYER_SHIRT_NUMBER);
		when(playerService.getPlayer(1)).thenReturn(servicePlayer);

		ResponseEntity<PlayerDto> responseEntity = playerController.getPlayer(1);

		assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
		PlayerDto responsePlayer = responseEntity.getBody();
		assertThat(responsePlayer.getId(), is(PLAYER_ID));
		assertThat(responsePlayer.getName(), is(PLAYER_NAME));
		assertThat(responsePlayer.getPosition(), is(PLAYER_POSITION));
		assertThat(responsePlayer.getShirtNumber(), is(PLAYER_SHIRT_NUMBER));
	}
}
