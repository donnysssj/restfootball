package de.planerio.developertest.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import de.planerio.developertest.dto.CountryDto;
import de.planerio.developertest.model.Country;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CountryControllerIntegrationTest {

	private static final String BASE_URL = "http://localhost:";
	private static final String COUNTRY_PATH = "/country";
	private static final String COUNTRY_NAME = "countryName";
	private static final String VALID_LANGUAGE = "en";
	private static final String INVALID_LANGUAGE = "English";

	@Autowired
	TestRestTemplate testRestTemplate;

	@LocalServerPort
	int port;

	private String countryBaseUrl;
	private HttpHeaders headers;

	@BeforeEach
	void commonSetup() throws URISyntaxException {
		countryBaseUrl = BASE_URL + port + COUNTRY_PATH;
		headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON.toString());
	}

	@Test
	void createCountry_withValidCountry_shouldReturnSuccess() {
		Country country = new Country().withName(COUNTRY_NAME).withLanguage(VALID_LANGUAGE);
		HttpEntity<Country> request = new HttpEntity<>(country, headers);

		ResponseEntity<CountryDto> result = testRestTemplate.postForEntity(countryBaseUrl, request, CountryDto.class);

		assertThat(result.getStatusCode(), is(HttpStatus.CREATED));

		CountryDto createdCountry = result.getBody();
		assertThat(createdCountry.getId(), notNullValue());
		assertThat(createdCountry.getName(), is(COUNTRY_NAME));
		assertThat(createdCountry.getLanguage(), is(VALID_LANGUAGE));

		testRestTemplate.delete(countryBaseUrl + "/" + createdCountry.getId());
	}

	@Test
	void createCountry_withInvalidLanguage_shouldReturnBadRequest() {
		Country country = new Country().withName(COUNTRY_NAME).withLanguage(INVALID_LANGUAGE);
		HttpEntity<Country> request = new HttpEntity<>(country, headers);

		ResponseEntity<CountryDto> result = testRestTemplate.postForEntity(countryBaseUrl, request, CountryDto.class);

		assertThat(result.getStatusCode(), is(HttpStatus.BAD_REQUEST));
	}

	@Test
	void updateCountry_withValidCountry_shouldReturnSuccess() {
		String updateCountryUrl = countryBaseUrl + "/1";
		CountryDto previousCountry = testRestTemplate.getForObject(updateCountryUrl, CountryDto.class);

		Country country = new Country().withId(1).withName(COUNTRY_NAME).withLanguage(VALID_LANGUAGE);
		HttpEntity<Country> request = new HttpEntity<>(country, headers);

		ResponseEntity<CountryDto> result = testRestTemplate.exchange(updateCountryUrl, HttpMethod.PUT, request,
				CountryDto.class);

		assertThat(result.getStatusCode(), is(HttpStatus.OK));

		CountryDto updatedCountry = result.getBody();
		assertThat(updatedCountry.getId(), is(previousCountry.getId()));
		assertThat(updatedCountry.getName(), is(country.getName()));
		assertThat(updatedCountry.getLanguage(), is(country.getLanguage()));
	}
}
